import { useState, useEffect, useRef, createRef } from "react";
import Modal from "react-modal";
import Plausible from "plausible-tracker";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCartShopping,
  faCircleCheck,
  faPlay,
  faVolumeHigh,
  faVolumeMute,
  faXmark,
} from "@fortawesome/free-solid-svg-icons";
// import { VIDEO_URL } from "../config";


Modal.setAppElement("#storySliderRoot");

const customStyles = {
  overlay: {
    backgroundColor: "rgba(0, 0, 0, 0.7)",
    zIndex: 100000000000,
    overflow: "hidden",
    // height: "-webkit-fill-available",
    height:'100%'
  },
  content: {
    top: "0%",
    left: "0%",
    right: "0%",
    bottom: "0%",
    padding: 0,
    backgroundColor: "transparent",
    border: "1ps solid transparent",
    overflow: "hidden",
    height: "100%",
    zIndex: 10,
  },
};

const productModalStyle = {
  products: {
    height: "fit-content",
    padding: "8px",
    cursor: "pointer",
    borderRadius: "5px",
    background: "rgba(0,0,0,0.7)",
    color: "white",
    fontSize: "16px",
    display: "flex",
    gap: "10px",
  },
};

function SwiperModal(prop) {
  const {
    isModalOpen,
    productId,
    setIsModalOpen,
    playlistResponse,
    viewportHeigth,
    controlsVideoRef,
    currentIndex,
    setCurrentIndex,
    trackEvent,
    setShowThumbnail,
    descriptionBoxRef,
    playlistID,
    setViewportHeight,
    currentVideo,
    setCurrentVideo,
    checkoutEnabled,
    setCheckoutEnabled,
    addToCartEnabled,
    setAddToCartEnabled,
    checkoutType,
    throughDraggable,
    throughSlider,
    handleCloseModal
  } = prop;

  const [selectedProductIndex, setSelectedProductIndex] = useState(0);
  const [selectedVariantIndex, setSelectedVariantIndex] = useState(0);
  const [moreDes, setMoreDes] = useState(false);
  const [varinatSelected, setVariantSelected] = useState(null);
  const [toggleDescription, setToggleDescription] = useState(false);
  const [isMute, setIsMute] = useState(false);
  const [cartCount, setCartCount] = useState(0);
  const [videoWidth, setVideoWidth] = useState(0);

  const ModalSwiperRef = useRef();
  const ProductSwiperRef = useRef();
  const SubVideoSwiperRef = useRef([]);
  const childModalRef = useRef(null);
  const playButtonRef = useRef([]);
  const descriptionTextRef = useRef(null);
  const checkoutGokwikRef = useRef(null);

  if (SubVideoSwiperRef.current.length === 0) {
    SubVideoSwiperRef.current = playlistResponse?.videos?.map((item) =>
      createRef()
    );
    playButtonRef.current = playlistResponse?.videos?.map((item) => createRef());
  }

  useEffect(() => {
    const updateVideWidth = () => {
      setVideoWidth(
        SubVideoSwiperRef.current[currentIndex]?.current?.offsetWidth
      );
    };

    window.addEventListener("resize", updateVideWidth);

    return () => window.removeEventListener("resize", updateVideWidth);
  });

  useEffect(() => {
    setTimeout(() => {
      document.body.style.overflow = "hidden";

      if (ModalSwiperRef.current) {
        // Start: Modal Swiper
        const swiperParams = {
          on: {
            init() {
              // ...
            },
            slideChangeTransitionEnd: function (event) {
              if(throughDraggable){
              const previousIndex = event.previousIndex
              const video = SubVideoSwiperRef.current[previousIndex].current;
            // console.log("Popup Played Video Time");
            // console.log({
            //   store: window.location.hostname,
            //   playlistId: playlistID,
            //   videoId: video.dataset.id,
            //   videoTitle: video.getAttribute('name'),
            //   playedTime: `${Math.round(video.currentTime)}s`
            // });
            trackEvent("Popup Played Video Time", {
              props: {
                  store: window.location.hostname,
                  playlistId: playlistID,
                  videoId: video.dataset.id,
                  videoTitle: video.getAttribute('name'),
                  playedTime: `${Math.round(video.currentTime)}s`
                },
            });
          }

          if(throughSlider){
            const previousIndex = event.previousIndex
              const video = SubVideoSwiperRef.current[previousIndex].current;
            // console.log("PDP Played Video Time");
            // console.log({
            //   store: window.location.hostname,
            //   playlistId: playlistID,
            //   videoId: video.dataset.id,
            //   videoTitle: video.getAttribute('name'),
            //   playedTime: `${Math.round(video.currentTime)}s`
            // });
            trackEvent("PDP Played Video Time", {
              props: {
                  store: window.location.hostname,
                  playlistId: playlistID,
                  videoId: video.dataset.id,
                  videoTitle: video.getAttribute('name'),
                  playedTime: `${Math.round(video.currentTime)}s`
                },
            });
          }

              setToggleDescription(false);
              setMoreDes(false);
              setVariantSelected(null); //Variant Selected is null on Change of Video Slider
              setIsMute(false); //On Change Video Is Not Muted

              if (window.innerWidth < 600) {
                ModalSwiperRef.current.swiper.navigation.prevEl.style.display =
                  "block";
                ModalSwiperRef.current.swiper.navigation.nextEl.style.display =
                  "block";
              }

              descriptionBoxRef.current.forEach((descRef) => {
                descRef.current.classList.add("bringin-slider-story-hidden");
              });

              event.slides.forEach((item, index) => {
                  if (item.classList.contains("swiper-slide-active")) {
                    setCurrentIndex(index); // Use Effect is called
                }
              });

              document
                .getElementById("productSwiper")
                .classList.remove("bringin-slider-story-hidden"); //Product Slider is shown
              ProductSwiperRef.current.swiper.slideTo(0, false); //Slided to zeroth index
            },
            destroy(event){
              if(throughDraggable){
              const activeVideoIndex = event.activeIndex
              const video = SubVideoSwiperRef.current[activeVideoIndex].current;
              // console.log("Popup Played Video Time");
              // console.log({
              //   store: window.location.hostname,
              //   playlistId: playlistID,
              //   videoId: video.dataset.id,
              //   videoTitle: video.getAttribute('name'),
              //   playedTime: `${Math.round(video.currentTime)}s`
              // });
              trackEvent("Popup Played Video Time", {
                props: {
                    store: window.location.hostname,
                    playlistId: playlistID,
                    videoId: video.dataset.id,
                    videoTitle: video.getAttribute('name'),
                    playedTime: `${Math.round(video.currentTime)}s`
                  },
              });
            }

            if(throughSlider){
              const activeVideoIndex = event.activeIndex
              const video = SubVideoSwiperRef.current[activeVideoIndex].current;
              // console.log("PDP Played Video Time");
              // console.log({
              //   store: window.location.hostname,
              //   playlistId: playlistID,
              //   videoId: video.dataset.id,
              //   videoTitle: video.getAttribute('name'),
              //   playedTime: `${Math.round(video.currentTime)}s`
              // });
              trackEvent("PDP Played Video Time", {
                props: {
                    store: window.location.hostname,
                    playlistId: playlistID,
                    videoId: video.dataset.id,
                    videoTitle: video.getAttribute('name'),
                    playedTime: `${Math.round(video.currentTime)}s`
                  },
              });
            }
              
            }
          },
        };

        // now we need to assign all parameters to Swiper element
        Object.assign(ModalSwiperRef.current, swiperParams);

        // and now initialize it
        ModalSwiperRef.current.initialize();

        // Current Video is played and Slided to the clicked thumbanail slide
        ModalSwiperRef.current.swiper.slideTo(currentIndex, false);
        SubVideoSwiperRef.current[currentIndex].current.play();

        // Setting the width of the product slider
        if (window.innerWidth < 1024) {
          document.getElementById(
            "productSwiper"
          ).style.width = `${window.innerWidth}px`;
        } else {
          document.getElementById("productSwiper").style.width = `${
            window.innerWidth * 0.33
          }px`;
        }

        // Changing color of Next and Prev Btn

        ModalSwiperRef.current.swiper.navigation.prevEl.style.color = "white";
        ModalSwiperRef.current.swiper.navigation.nextEl.style.color = "white";

        // End:- Modal swiper

        //Start: -- Product Swiper
        if (ProductSwiperRef.current) {
          const swiperParams = {
            breakpoints: {
              0: {
                slidesPerView: 1,
                spaceBetween: 10,
              },
              350: {
                slidesPerView: 2,
                spaceBetween: 10,
              },
            },
            on: {
              init() {
                // ...
              },
              slideChangeTransitionEnd: function (event) {
                let videoDetailsStr;
                event?.el?.childNodes?.forEach((item) => {
                  if (item?.dataset?.videodetails) {
                    videoDetailsStr = item.dataset.videodetails;
                  }
                });

                if (videoDetailsStr) {
                  const videoDetailsJSON = JSON.parse(videoDetailsStr);

                  if (throughDraggable) {
                    // console.log("Popup Product Slider Click");
                    // console.log({
                    //   store: window.location.hostname,
                    //   productId,
                    //   videoId: videoDetailsJSON?.id,
                    //   videoTitle: videoDetailsJSON?.title,
                    // });
                    trackEvent("Popup Product Slider Click", {
                      props: {
                        store: window.location.hostname,
                        productId,
                        videoId: videoDetailsJSON?.id,
                        videoTitle: videoDetailsJSON?.title,
                      },
                    });
                  }

                  if(throughSlider){
                  //   console.log("PDP Product Slider Click");
                  // console.log({
                  //   store: window.location.hostname,
                  //   productId,
                  //   videoId: videoDetailsJSON?.id,
                  //   videoTitle: videoDetailsJSON?.title,
                  // });
                  trackEvent("PDP Product Slider Click", {
                    props: {
                      store: window.location.hostname,
                      productId,
                      videoId: videoDetailsJSON?.id,
                      videoTitle: videoDetailsJSON?.title,
                    },
                  });
                  }
                }
              },
            },
          };

          // now we need to assign all parameters to Swiper element
          Object.assign(ProductSwiperRef.current, swiperParams);

          // and now initialize it
          ProductSwiperRef.current.initialize();

          ProductSwiperRef.current.swiper.navigation.prevEl.style.color =
            "white";
          ProductSwiperRef.current.swiper.navigation.nextEl.style.color =
            "white";
          ProductSwiperRef.current.swiper.navigation.nextEl.style.setProperty(
            "--swiper-navigation-size",
            "12px"
          );
          ProductSwiperRef.current.swiper.navigation.prevEl.style.setProperty(
            "--swiper-navigation-size",
            "12px"
          );
          ProductSwiperRef.current.swiper.navigation.prevEl.style.fontWeight =
            "bold";
          ProductSwiperRef.current.swiper.navigation.nextEl.style.fontWeight =
            "bold";
        }
        //End: -- Product Swiper

        //Start:-- Setting the Video Width
        SubVideoSwiperRef?.current[currentIndex]?.current?.addEventListener(
          "loadedmetadata",
          () => {
            if (
              SubVideoSwiperRef?.current[currentIndex]?.current?.offsetWidth
            ) {
              const width =
                SubVideoSwiperRef.current[currentIndex].current.offsetWidth;
              setVideoWidth(width);
            }
          }
        );
        // End:-- Setting the Video Width
      }
    }, 0);
  }, []);

  useEffect(() => {
    const updateVideWidth = () => {
      setVideoWidth(
        SubVideoSwiperRef.current[currentIndex]?.current?.offsetWidth
      );
    };

    window.addEventListener("resize", updateVideWidth);

    return () => window.removeEventListener("resize", updateVideWidth);
  });

  useEffect(() => {
    // Playing the current window and Pausing rest
    if (currentIndex !== null) {
      // Setting the video
      const currentVid = playlistResponse?.videos[currentIndex];
      setCurrentVideo({ ...currentVid });

      // Hiding the play button;
      playButtonRef?.current[currentIndex]?.current?.classList?.add("bringin-slider-story-hidden");

      SubVideoSwiperRef.current.forEach((video, index) => {
        if (index === currentIndex) {
          video.current?.play();
        } else {
          if (video.current) video.current.currentTime = 0;
          video.current?.pause();
        }
      });

      if (SubVideoSwiperRef?.current[currentIndex]?.current?.offsetWidth) {
        const width =
          SubVideoSwiperRef.current[currentIndex].current.offsetWidth;
        setVideoWidth(width);
      }

      ProductSwiperRef.current?.swiper?.updateSlides(); //Update the slides
    }
  }, [currentIndex]);

  useEffect(() => {
    if (currentVideo) {
      // Plausible
      if(throughDraggable){

      // console.log("Popup Video Click");
      // console.log({
      //   store: window.location.hostname,
      //   productId,
      //   videoId: currentVideo?.id,
      //   videoTitle: currentVideo?.title,
      // });
      trackEvent("Popup Video Click", {
        props: {
          store: window.location.hostname,
          productId,
          videoId: currentVideo?.id,
          videoTitle: currentVideo?.title,
        },
      });
              
    }

    if(throughSlider){
      // console.log("PDP Video Click");
      // console.log({
      //   store: window.location.hostname,
      //   productId,
      //   videoId: currentVideo?.id,
      //   videoTitle: currentVideo?.title,
      // });
      trackEvent("PDP Video Click", {
        props: {
          store: window.location.hostname,
          productId,
          videoId: currentVideo?.id,
          videoTitle: currentVideo?.title,
        },
      });
    }
    }
  }, [currentVideo]);

  useEffect(() => {
    if (varinatSelected) {
      const currentProduct =
        playlistResponse?.videos[currentIndex].products[selectedProductIndex];

      if(throughDraggable){
      // console.log("Popup Add to Cart");
      // console.log({
      //   store: window.location.hostname,
      //   productId,
      //   currentProductId: currentProduct?.product_id,
      //   productName: currentProduct?.title,
      //   sku: currentProduct?.variants[selectedVariantIndex]?.sku,
      // });
      trackEvent("Popup Add to cart", {
        props: {
          store: window.location.hostname,
          productId,
          currentProductId: currentProduct?.product_id,
          productName: currentProduct?.title,
          sku: currentProduct?.variants[selectedVariantIndex]?.sku,
        },
      });
    }

    if(throughSlider){
      // console.log("PDP Add to Cart");
      // console.log({
      //   store: window.location.hostname,
      //   productId,
      //   currentProductId: currentProduct?.product_id,
      //   productName: currentProduct?.title,
      //   sku: currentProduct?.variants[selectedVariantIndex]?.sku,
      // });
      trackEvent("PDP Add to cart", {
        props: {
          store: window.location.hostname,
          productId ,
          currentProductId: currentProduct?.product_id,
          productName: currentProduct?.title,
          sku: currentProduct?.variants[selectedVariantIndex]?.sku,
        },
      });
    }
    }
  }, [varinatSelected]);

  useEffect(() => {
    if (!moreDes && descriptionTextRef.current) {
      // descriptionTextRef.current.scroll({
      //   top: 0,
      //   behavior: 'smooth'
      // })
    }
  }, [moreDes]);

  const handleOpenDescription = (event, index) => {
    event.preventDefault();
    setSelectedProductIndex(index);
    if (window.innerWidth < 600) {
      ModalSwiperRef.current.swiper.navigation.prevEl.style.display = "none";
      ModalSwiperRef.current.swiper.navigation.nextEl.style.display = "none";
    }
    document.getElementById("productSwiper").classList.add("bringin-slider-story-hidden");
    descriptionBoxRef.current.forEach((descRef, i) => {
      if (i === currentIndex) {
        descRef.current.classList.remove("bringin-slider-story-hidden");
        descRef.current.style.width = `${SubVideoSwiperRef.current[currentIndex].current.clientWidth}px`;
      } else {
        descRef.current.classList.add("bringin-slider-story-hidden");
      }
    });

    const currentProduct = playlistResponse?.videos[currentIndex]?.products[index];
    if(throughDraggable){
    // console.log("Popup Product Detail Open");
    // console.log({
    //   store: window.location.hostname,
    //   productId,
    //   videoId: currentVideo?.id,
    //   videoTitle: currentVideo?.title,
    //   currentProductId: currentProduct?.id,
    //   productName: currentProduct?.title,
    //   sku: currentProduct?.variants[0]?.sku,
    // });
    trackEvent("Popup Product Detail Open", {
      props: {
        store: window.location.hostname,
        productId,
        videoId: currentVideo?.id,
        videoTitle: currentVideo?.title,
        currentProductId: currentProduct?.id,
        productName: currentProduct?.title,
        sku: currentProduct?.variants[0]?.sku,
      },
    });
    }

    if(throughSlider){
    //   console.log("PDP Product Detail Open");
    // console.log({
    //   store: window.location.hostname,
    //   productId,
    //   videoId: currentVideo?.id,
    //   videoTitle: currentVideo?.title,
    //   currentProductId: currentProduct?.id,
    //   productName: currentProduct?.title,
    //   sku: currentProduct?.variants[0]?.sku,
    // });
    trackEvent("PDP Product Detail Open", {
      props: {
        store: window.location.hostname,
        productId,
        videoId: currentVideo?.id,
        videoTitle: currentVideo?.title,
        currentProductId: currentProduct?.id,
        productName: currentProduct?.title,
        sku: currentProduct?.variants[0]?.sku,
      },
    });
    }
  };

  const handleCloseDesc = (event) => {
    event.preventDefault();

    if (window.innerWidth < 600) {
      ModalSwiperRef.current.swiper.navigation.prevEl.style.display = "block";
      ModalSwiperRef.current.swiper.navigation.nextEl.style.display = "block";
    }

    descriptionBoxRef.current[currentIndex].current.classList.add("bringin-slider-story-hidden");
    document.getElementById("productSwiper").classList.remove("bringin-slider-story-hidden");
    setSelectedVariantIndex(0); //Variant is set to default index 0
    setVariantSelected(null); // Variant is set to null
    setToggleDescription(false); //Description Box is Set to Default Mode
  };

  const handleVariantChange = (event, variant, index) => {
    event.target.checked = true;
    setSelectedVariantIndex(index);
  };

  const handleAddToCart = (event, index) => {
    event.preventDefault();
    setToggleDescription(true);
    const variantId =
      playlistResponse?.videos?.[currentIndex]?.products[selectedProductIndex]
        ?.variants[0]?.id;
    document.getElementById(`btnradio_${index}_${variantId}`).checked = true;
  };

  const handleDone = (index) => {
    try {
      const variant =
        playlistResponse?.videos[index]?.products[selectedProductIndex]?.variants[
          selectedVariantIndex
        ];
      const payload = {
        id: variant?.variant_id,
        quantity: 1,
      };

      // Start:-- Testing Mode
      // setToggleDescription(false);
      // setVariantSelected({ ...variant });
      // End:-- Testing Mode

      // Start: Production Mode
      axios
        .post("/cart/add.js", payload)
        .then((res) => {
          if (res?.status === 200) {
            const count = res.data.quantity;
            count && setCartCount(count);
            setToggleDescription(false);
            setVariantSelected({ ...variant });
          }
        })
        .catch((err) => {});
      // End: Production Mode
    } catch (err) {}
  };

  const handleMute = (event, index) => {
    event.preventDefault();
    try {
      if (isMute) {
        if(throughDraggable){
        // console.log("Popup Video Unmuted");
        // console.log({
        //   store: window.location.hostname,
        //   productId,
        //   videoId: currentVideo?.id,
        //   videoTitle: currentVideo.title,
        // });
        trackEvent("Popup Video Unmuted", {
          props: {
            store: window.location.hostname,
            productId,
            videoId: currentVideo?.id,
            videoTitle: currentVideo.title,
          },
        });

      }

      if(throughSlider){
        // console.log("PDP Video Unmuted");
        // console.log({
        //   store: window.location.hostname,
        //   productId,
        //   videoId: currentVideo?.id,
        //   videoTitle: currentVideo.title,
        // });
        trackEvent("PDP Video Unmuted", {
          props: {
            store: window.location.hostname,
            productId,
            videoId: currentVideo?.id,
            videoTitle: currentVideo.title,
          },
        });
      }


      } else {

        if(throughDraggable){
        // console.log("Popup Video Muted");
        // console.log({
        //   store: window.location.hostname,
        //   productId,
        //   videoId: currentVideo?.id,
        //   videoTitle: currentVideo.title,
        // });
        trackEvent("Popup Video Muted", {
          props: {
            store: window.location.hostname,
            productId,
            videoId: currentVideo?.id,
            videoTitle: currentVideo.title,
          },
        });
      }

      if(throughSlider){
        // console.log("PDP Video Muted");
        // console.log({
        //   store: window.location.hostname,
        //   productId,
        //   videoId: currentVideo?.id,
        //   videoTitle: currentVideo.title,
        // });
        trackEvent("PDP Video Muted", {
          props: {
            store: window.location.hostname,
            productId,
            videoId: currentVideo?.id,
            videoTitle: currentVideo.title,
          },
        });
      }
      }
      setIsMute((prev) => !prev);
      SubVideoSwiperRef.current[index].current.muted =
        !SubVideoSwiperRef.current[index].current.muted;
    } catch (err) {}
  };

  const handlePausePlayVideo = (index) => {
    const isPaused = SubVideoSwiperRef.current[currentIndex].current.paused;
    if (isPaused) {
      SubVideoSwiperRef.current[currentIndex].current.play();
      playButtonRef.current[index].current.classList.add("bringin-slider-story-hidden");
      
      if(throughDraggable){
      // console.log("Popup Video Play");
      // console.log({
      //   store: window.location.hostname,
      //   productId,
      //   videoId: currentVideo?.id,
      //   videoTitle: currentVideo?.title,
      // });
      trackEvent("Popup Video Play", {
        props: {
          store: window.location.hostname,
          productId,
          videoId: currentVideo?.id,
          videoTitle: currentVideo?.title,
        },
      });
    }

    if(throughSlider){
      // console.log("PDP Video Play");
      // console.log({
      //   store: window.location.hostname,
      //   productId,
      //   videoId: currentVideo?.id,
      //   videoTitle: currentVideo?.title,
      // });
      trackEvent("PDP Video Play", {
        props: {
          store: window.location.hostname,
          productId,
          videoId: currentVideo?.id,
          videoTitle: currentVideo?.title,
        },
      });
    }

    } else {
      SubVideoSwiperRef.current[currentIndex].current.pause();
      if(throughDraggable){
      // console.log("Popup Video Pause");
      // console.log({
      //   store: window.location.hostname,
      //   productId,
      //   videoId: currentVideo?.id,
      //   videoTitle: currentVideo?.title,
      // });
      trackEvent("Popup Video Pause", {
        props: {
          store: window.location.hostname,
          productId,
          videoId: currentVideo?.id,
          videoTitle: currentVideo?.title,
        },
      });
    }

    if(throughSlider){
      //  console.log("PDP Video Pause");
      // console.log({
      //   store: window.location.hostname,
      //   productId,
      //   videoId: currentVideo?.id,
      //   videoTitle: currentVideo?.title,
      // });
      trackEvent("PDP Video Pause", {
        props: {
          store: window.location.hostname,
          productId,
          videoId: currentVideo?.id,
          videoTitle: currentVideo?.title,
        },
      });
    }
      playButtonRef.current[index].current.classList.remove("bringin-slider-story-hidden");
    }
  };

  const handleRedirectProductDespcriptionPage = () => {
    const currentProduct =
      playlistResponse?.videos[currentIndex].products[selectedProductIndex];

    if(throughDraggable){
      // console.log("Popup Product Redirect to PDP");
      // console.log({
      //   store: window.location.hostname,
      //   productId,
      //   videoId: currentVideo?.id,
      //   videoTitle: currentVideo?.title,
      //   currentProductId: currentProduct?.id,
      //   productName: currentProduct?.title,
      //   sku: currentProduct?.variants[selectedVariantIndex]?.sku,
      // });
      trackEvent("Popup Product Redirect to PDP", {
        props: {
          store: window.location.hostname,
          productId,
          videoId: currentVideo?.id,
          videoTitle: currentVideo?.title,
          currentProductId: currentProduct?.id,
          productName: currentProduct?.title,
          sku: currentProduct?.variants[selectedVariantIndex]?.sku,
        },
      });
    }

    if(throughSlider){
    //   console.log("PDP Slider Product Redirect");
    // console.log({
    //   store: window.location.hostname,
    //   productId,
    //   videoId: currentVideo?.id,
    //   videoTitle: currentVideo?.title,
    //   currentProductId: currentProduct?.id,
    //   productName: currentProduct?.title,
    //   sku: currentProduct?.variants[selectedVariantIndex]?.sku,
    // });
    trackEvent("PDP Slider Product Redirect", {
      props: {
        store: window.location.hostname,
        productId,
        videoId: currentVideo?.id,
        videoTitle: currentVideo?.title,
        currentProductId: currentProduct?.id,
        productName: currentProduct?.title,
        sku: currentProduct?.variants[selectedVariantIndex]?.sku,
      },
    });
    }
   
  };

  return (
    <Modal
      isOpen={isModalOpen}
      onRequestClose={handleCloseModal}
      style={customStyles}
    >
      <section
        style={{ height: `${viewportHeigth}px` }}
        ref={childModalRef}
        className="bringin-slider-story-relative bringin-slider-story-w-full xl:bringin-slider-story-w-[900px] bringin-slider-story-m-auto"
        id="childModalSwiper"
      >
        <swiper-container
          ref={ModalSwiperRef}
          slides-per-view={"1"}
          spaceBetween={0}
          navigation="true"
          style={{ height: `${viewportHeigth}px` }}
          init="false"
        >
          {playlistResponse?.videos?.map((video, index) => {
            return (
              <swiper-slide style={{width:"830px"}} key={video.id}>
                <div className="bringin-slider-story-flex bringin-slider-story-flex-col bringin-slider-story-justify-center bringin-slider-story-h-full bringin-slider-story-items-center bringin-slider-story-relative">
                  <article className="bringin-slider-story-absolute bringin-slider-story-w-full bringin-slider-story-flex bringin-slider-story-justify-center bringin-slider-story-text-white bringin-slider-story-top-[1%] bringin-slider-story-z-10">
                    {/* <div className='bringin-slider-story-flex bringin-slider-story-justify-between bringin-slider-story-font-bold bringin-slider-story-w-[40%] md:bringin-slider-story-w-[50%] lg:bringin-slider-story-w-[80%] 3xl:bringin-slider-story-w-[95%] bringin-slider-story-text-lg 2xl:bringin-slider-story-text-3xl'> */}
                    <div
                      ref={controlsVideoRef.current[index]}
                      className="bringin-slider-story-flex bringin-slider-story-justify-between bringin-slider-story-font-bold bringin-slider-story-px-2  bringin-slider-story-text-lg 2xl:bringin-slider-story-text-3xl"
                      style={{ width: `${videoWidth}px` }}
                    >
                      <div
                        className="bringin-slider-story-cursor-pointer bringin-slider-story-w-12 bringin-slider-story-h-12 bringin-slider-story-rounded-full bringin-slider-story-bg-black/20 bringin-slider-story-flex bringin-slider-story-justify-center bringin-slider-story-items-center"
                        onClick={(event) => handleMute(event, index)}
                      >
                        {isMute ? (
                          <FontAwesomeIcon icon={faVolumeMute} />
                        ) : (
                          <FontAwesomeIcon icon={faVolumeHigh} />
                        )}
                      </div>
                      <div
                        className="bringin-slider-story-cursor-pointer bringin-slider-story-w-12 bringin-slider-story-h-12 bringin-slider-story-rounded-full bringin-slider-story-bg-black/20 bringin-slider-story-flex bringin-slider-story-justify-center bringin-slider-story-items-center"
                        onClick={handleCloseModal}
                      >
                        <FontAwesomeIcon icon={faXmark} />
                      </div>
                    </div>
                  </article>

                  <video
                    name={video?.title}
                    data-id={video?.id}
                    autoPlay={false}
                    ref={SubVideoSwiperRef.current[index]}
                    playsInline
                    className="bringin-slider-story-h-screen bringin-slider-story-block bringin-slider-story-max-w-full bringin-slider-story-object-contain bringin-slider-story-overflow-clip"
                    preload="meta"
                    onClick={() => handlePausePlayVideo(index)}
                  >
                    <source
                      // src={`${VIDEO_URL}/${video.video_url}`}
                      type="video/mp4"
                    />
                  </video>
                  {/* <article ref={descriptionBoxRef} style={{width:"70%"}} className='bringin-slider-story-bg-black/70 bringin-slider-story-absolute bringin-slider-story-bottom-0 bringin-slider-story-text-sm bringin-slider-story-translate-x-20 bringin-slider-story-rounded-t-xl bringin-slider-story-pt-5 bringin-slider-story-pb-12 bringin-slider-story-pl-4 bringin-slider-story-pr-4 bringin-slider-story-text-white  bringin-slider-story-descriptionBoxModal'> */}
                  <article className="bringin-slider-story-w-full bringin-slider-story-max-h-[75vh] bringin-slider-story-flex bringin-slider-story-justify-center bringin-slider-story-absolute bringin-slider-story-bottom-0 xl:bringin-slider-story-bottom-[0%] 2xl:bringin-slider-story-bottom-[0%] 3xl:bringin-slider-story-bottom-[0%] bringin-slider-story-text-sm bringin-slider-story-text-white">
                    {/* <div ref={descriptionBoxRef.current[index]} className='bringin-slider-story-hidden bringin-slider-story-w-2/5 sm:bringin-slider-story-w-[46%] md:bringin-slider-story-w-[58%] lg:bringin-slider-story-w-[70%] xl:bringin-slider-story-w-[64%] 2xl:bringin-slider-story-w-[55%] 3xl:bringin-slider-story-w-[70%] bringin-slider-story-bg-black/80 bringin-slider-story-h-full bringin-slider-story-rounded-t-2xl bringin-slider-story-pt-4 bringin-slider-story-pb-20 bringin-slider-story-px-4'> */}
                    <div
                      ref={descriptionBoxRef.current[index]}
                      className="bringin-slider-story-hidden bringin-slider-story-w-2/5 bringin-slider-story-bg-black/80 bringin-slider-story-rounded-t-2xl bringin-slider-story-pt-4 bringin-slider-story-pb-[30px] bringin-slider-story-px-4"
                    >
                      <div className="bringin-slider-story-flex bringin-slider-story-justify-between bringin-slider-story-items-center bringin-slider-story-pb-2 bringin-slider-story-border-2 bringin-slider-story-border-solid bringin-slider-story-border-transparent bringin-slider-story-border-b-white  bringin-slider-story-pr-3 bringin-slider-story-font-bold">
                        <div></div>
                        <div className="bringin-slider-story-tracking-wide bringin-slider-story-text-lg">Shop</div>
                        <button
                          className="bringin-slider-story-cursor-pointer bringin-slider-story-border-none bringin-slider-story-bg-transparent bringin-slider-story-text-white bringin-slider-story-text-xl"
                          onClick={handleCloseDesc}
                        >
                          <FontAwesomeIcon icon={faXmark} />
                        </button>
                      </div>
                      <a
                        className="bringin-slider-story-w-full bringin-slider-story-no-underline bringin-slider-story-text-white"
                        href={`/products/${video?.products[selectedProductIndex]?.handle}`}
                        onClick={handleRedirectProductDespcriptionPage}
                        target="_blank"
                        rel="noreferrer"
                      >
                        <div className="bringin-slider-story-flex bringin-slider-story-items-center bringin-slider-story-gap-2 bringin-slider-story-text-sm bringin-slider-story-pt-2 bringin-slider-story-font-bold">
                          <img
                            src={
                              video?.products[selectedProductIndex]?.images[0]
                                ?.src
                            }
                            alt={video?.products[selectedProductIndex]?.title}
                            className="bringin-slider-story-rounded"
                            width="50px"
                            height="50px"
                          />
                          <div>
                            {video?.products[selectedProductIndex]?.title}{" "}
                            <br />
                            {`₹ ${video?.products[selectedProductIndex]?.variants[selectedVariantIndex]?.price}`}
                          </div>
                        </div>
                      </a>
                      <section
                        className="bringin-slider-story-transition  bringin-slider-story-duration-150 bringin-slider-story-ease-out"
                        style={{
                          display: toggleDescription ? "none" : "block",
                        }}
                      >
                        <div className="bringin-slider-story-pt-2 bringin-slider-story-text-sm">
                          <div className="bringin-slider-story-font-bold bringin-slider-story-mb-2">Description</div>
                          <div
                            ref={descriptionTextRef}
                            className={`bringinDescriptionBox bringin-slider-story-transition-height bringin-slider-story-text-white bringin-slider-story-duration-200 bringin-slider-story-ease-linear  bringin-slider-story-text-justify
                              ${
                                moreDes
                                  ? "bringin-slider-story-max-h-52 bringin-slider-story-overflow-y-scroll"
                                  : "bringin-slider-story-max-h-16 bringin-slider-story-overflow-y-hidden"
                              }`}
                            dangerouslySetInnerHTML={{
                              __html:
                                video?.products[selectedProductIndex]
                                  ?.description,
                            }}
                          ></div>
                        </div>
                        <div
                          className="bringin-slider-story-text-center bringin-slider-story-cursor-pointer bringin-slider-story-mt-4 bringin-slider-story-font-bold hover:bringin-slider-story-text-white/80"
                          onClick={() => setMoreDes((prev) => !prev)}
                        >
                          {moreDes ? "Less" : "More"}
                        </div>
                        {varinatSelected ? (
                          <div className="bringin-slider-story-w-full bringin-slider-story-flex bringin-slider-story-gap-4">
                            <a
                              className={`${
                                checkoutEnabled === "true" ? "bringin-slider-story-w-1/2" : "bringin-slider-story-w-full"
                              } bringin-slider-story-no-underline`}
                              onClick={handleRedirectProductDespcriptionPage}
                              href="/cart"
                            >
                              <button
                                id="bringinAddedToCart"
                                className="bringin-slider-story-w-full bringin-slider-story-cursor-pointer bringin-slider-story-border-none bringin-slider-story-bg-white bringin-slider-story-text-black  bringin-slider-story-mt-2 bringin-slider-story-mb-2 hover:bringin-slider-story-bg-white/90"
                              >
                                <FontAwesomeIcon
                                  color="#198754"
                                  icon={faCircleCheck}
                                />{" "}
                                Added to Cart
                              </button>
                            </a>
                            {checkoutType === "gokwik" ? (
                              <div className="bringin-slider-story-gokwik-checkout bringin-slider-story-mb-0 bringin-slider-story-w-1/2">
                                <button
                                  id="bringinCheckout"
                                  type="button"
                                  className="bringin-slider-story-w-full bringin-slider-story-cursor-pointer bringin-slider-story-border-none bringin-slider-story-mt-2 bringin-slider-story-mb-2 bringin-slider-story-relative"
                                  ref={checkoutGokwikRef}
                                  // onClick= "onCheckoutClick(this)"
                                  onClick={() =>
                                    window.onCheckoutClick(
                                      checkoutGokwikRef.current
                                    )
                                  }
                                >
                                  <span className="">
                                    <span>
                                      Checkout{" "}
                                      <FontAwesomeIcon icon={faCartShopping} />{" "}
                                      <span
                                        id="cartCountBadge"
                                        style={{
                                          top: "-13px",
                                          right: "-12px",
                                        }}
                                        className="bringin-slider-story-absolute bringin-slider-story-rounded-full  bringin-slider-story-w-7 bringin-slider-story-h-7 bringin-slider-story-flex bringin-slider-story-justify-center bringin-slider-story-items-center bringin-slider-story-right-0 bringin-slider-story-border bringin-slider-story-border-white"
                                      >
                                        {cartCount}
                                      </span>
                                    </span>
                                    <span></span>
                                  </span>
                                  <span className="bringin-slider-story-pay-opt-icon bringin-slider-story-hidden">
                                    {/* <img src="https://cdn.gokwik.co/v4/images/upi-icons.svg" />
                                      <img src="https://cdn.gokwik.co/v4/images/right-arrow.svg" /> */}
                                  </span>
                                  <div className="bringin-slider-story-hidden">
                                    <div className="bringin-slider-story-cir-loader">Loading..</div>
                                  </div>
                                </button>
                              </div>
                            ) : (
                              <a
                                className={`${
                                  checkoutEnabled === "true"
                                    ? "bringin-slider-story-w-1/2"
                                    : "bringin-slider-story-hidden"
                                } bringin-slider-story-no-underline`}
                                href="/checkout"
                              >
                                <button
                                  id="bringinCheckout"
                                  className="bringin-slider-story-w-full bringin-slider-story-cursor-pointer bringin-slider-story-border-none bringin-slider-story-mt-2 bringin-slider-story-mb-2 bringin-slider-story-relative"
                                >
                                  Checkout{" "}
                                  <FontAwesomeIcon icon={faCartShopping} />{" "}
                                  <span
                                    id="cartCountBadge"
                                    style={{
                                      top: "-13px",
                                      right: "-12px",
                                    }}
                                    className="bringin-slider-story-absolute bringin-slider-story-rounded-full  bringin-slider-story-w-7 bringin-slider-story-h-7 bringin-slider-story-flex bringin-slider-story-justify-center bringin-slider-story-items-center bringin-slider-story-right-0 bringin-slider-story-border bringin-slider-story-border-white"
                                  >
                                    {cartCount}
                                  </span>
                                </button>
                              </a>
                            )}
                          </div>
                        ) : !varinatSelected && addToCartEnabled === "true" ? (
                          <button
                            id="bringinAddToCart"
                            className="bringin-slider-story-w-full bringin-slider-story-cursor-pointer bringin-slider-story-border-none bringin-slider-story-mt-2 bringin-slider-story-mb-2"
                            onClick={(event) => handleAddToCart(event, index)}
                          >
                            Add to Cart
                          </button>
                        ) : (
                          ""
                        )}
                      </section>
                      <section
                        style={{
                          display: !toggleDescription ? "none" : "block",
                        }}
                        className="bringin-slider-story-mt-4"
                      >
                        <article className="bringin-slider-story-flex bringin-slider-story-justify-center bringin-slider-story-flex-wrap bringin-slider-story-gap-3">
                          {video?.products[selectedProductIndex]?.variants?.map(
                            (variant, idt) => {
                              return (
                                <div key={variant?.id} className="bringin-slider-story-mt-2">
                                  <input
                                    type="radio"
                                    value={variant.variant_id}
                                    className="btnVariantRadio bringin-slider-story-hidden"
                                    name={`btnradio_${index}`}
                                    id={`btnradio_${index}_${variant.id}`}
                                    onChange={(event) =>
                                      handleVariantChange(event, variant, idt)
                                    }
                                  />
                                  <label
                                    className="btnVariant bringin-slider-story-cursor-pointer"
                                    htmlFor={`btnradio_${index}_${variant.id}`}
                                  >
                                    {variant.title}
                                  </label>
                                </div>
                              );
                            }
                          )}
                        </article>
                        <div className="bringin-slider-story-flex bringin-slider-story-justify-center bringin-slider-story-mt-3">
                          <button
                            className=" bringin-slider-story-w-fit bringin-slider-story-cursor-pointer bringin-slider-story-text-black bringin-slider-story-p-2 bringin-slider-story-text-base bringin-slider-story-bg-white bringin-slider-story-border-none bringin-slider-story-rounded bringin-slider-story-mt-2 bringin-slider-story-mb-2 bringin-slider-story-px-5 bringin-slider-story-py-2  hover:bringin-slider-story-bg-white/90"
                            onClick={() => handleDone(index)}
                          >
                            Done
                          </button>
                        </div>
                      </section>
                    </div>
                  </article>

                  <div
                    ref={playButtonRef.current[index]}
                    className="bringin-slider-story-hidden bringin-slider-story-text-8xl xl:bringin-slider-story-text-[10rem] bringin-slider-story-text-white/40 bringin-slider-story-absolute bringin-slider-story-top-2/4  bringin-slider-story-left-2/4 bringin-slider-story-translate-x-[-50%] bringin-slider-story-translate-y-[-50%]"
                    onClick={() => handlePausePlayVideo(index)}
                  >
                    <FontAwesomeIcon icon={faPlay} />
                  </div>
                </div>
              </swiper-slide>
            );
          })}
        </swiper-container>
        <article
          id="productSwiper"
          style={{
            zIndex: 1,
            transform: "translateX(-50%)",
            width: `${videoWidth}px`,
          }}
          className="bringin-slider-story-absolute bringin-slider-story-bottom-[2%] bringin-slider-story-left-2/4 "
        >
          <swiper-container
            className="mySwiper"
            ref={ProductSwiperRef}
            navigation="true"
            centered-slides="true"
            init="false"
          >
            {playlistResponse?.videos[currentIndex]?.products?.map(
              (prod, index) => {
                const obj = {
                  id: playlistResponse?.videos[currentIndex]?.id,
                  title: playlistResponse?.videos[currentIndex]?.title,
                };
                return (
                  <swiper-slide
                    style={productModalStyle.products}
                    key={prod?.id}
                    data-videoDetails={JSON.stringify(obj)}
                    onClick={(event) => handleOpenDescription(event, index)}
                  >
                    <img
                      className="bringin-slider-story-w-[60px] bringin-slider-story-h-[60px] bringin-slider-story-rounded bringin-slider-story-object-cover "
                      alt={prod?.images[0]?.alt}
                      src={prod?.images[0]?.src}
                    />
                    <div className="bringin-slider-story-flex bringin-slider-story-flex-col bringin-slider-story-justify-between bringin-slider-story-w-[60%] xxlg:bringin-slider-story-w-[63%] 5xl:bringin-slider-story-w-[70%] bringin-slider-story-text-[14px] ">
                      <div className="prodTitle bringin-slider-story-overflow-hidden bringin-slider-story-whitespace-nowrap bringin-slider-story-text-ellipsis ">
                        {prod?.title}
                      </div>
                      <div>{`₹ ${prod?.variants[0]?.price} `}</div>
                      <div>Shop Now {`>`}</div>
                    </div>
                  </swiper-slide>
                );
              }
            )}
          </swiper-container>
        </article>
      </section>
    </Modal>
  );
}


export default SwiperModal
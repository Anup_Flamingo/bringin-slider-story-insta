import React, { useEffect, useRef, useState } from 'react';
// import function to register Swiper custom elements
import { register } from 'swiper/element/bundle';
import SwiperModal from './SwiperModal';
import Plausible from "plausible-tracker";
import { CND_URL} from '../config';
import PlaylistModal from './PlaylistModal';


// register Swiper custom elements
register()

function StorySlider(props) {
  const {playlistResponse, mainThemeColor, viewPortHeight, checkoutEnabled, addToCartEnabled, checkoutType} =  props;

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [currentPlaylistId, setCurrentPlaylistId] = useState(null);
  const [currentActivePlaylistIndex, setCurrentActivePlaylistIndex] = useState(null);


  
  const swiperStorySliderRef = useRef(null);  
  const VideoSwiperRef = useRef([]);

  useEffect(()=>{
    window.addEventListener("resize",()=>{
      updateSlides()
    })  
  },[])

  useEffect(()=>{ 

    setTimeout(()=>{
      if(swiperStorySliderRef.current){

       updateSlides()

        // Styling the Swiper-Slide for Shadow Root
          swiperStorySliderRef.current?.swiper?.slides?.forEach(slide=>{
            slide.style.width = "100px"
            slide.style.height = "100px"
            slide.style.borderRadius = "50%",
            slide.style.padding = "4px"
            if(mainThemeColor){
              slide.style.border = `4px solid ${mainThemeColor}`
            }else{
              slide.style.background = "linear-gradient(rgb(231,0,231), rgb(255,115,0))"

            }
          })

      }
    },0)


  },[])

  function updateSlides (){
    if(swiperStorySliderRef.current){

      const windowWidth = window.innerWidth;
      const slidesPerViewport  = windowWidth> 1300? 11: windowWidth/120
      const swiperSliderParams = {
        slidesPerView : slidesPerViewport
      }
      
      // now we need to assign all parameters to Swiper element
      Object.assign(swiperStorySliderRef.current, swiperSliderParams);
      // and now initialize it
      swiperStorySliderRef.current.swiper.update();
      
    }
  }
  
  const handleOpenModal = (id,index)=>{
    setCurrentPlaylistId(id)
    setCurrentActivePlaylistIndex(index)
    setIsModalOpen(true)
  }
  const handleCloseModal = () => {
    setIsModalOpen(false)
  };




  return (
    <div className='bringinStroySldierContainer'>
      <swiper-container
        ref={swiperStorySliderRef}
        init = "true"
        space-between="20"
      >
       {
        playlistResponse.map((playlist, idx)=>{
          return(

            <swiper-slide key={playlist?.id} style={
             {
              width: "100px !important",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              cursor: "pointer"
             }
            }>
              <div style={{backgroundColor: "red", border: !mainThemeColor?"4px solid white" :"none"}} className='bringin-slider-story-m-[1px] bringin-slider-story-w-full bringin-slider-story-h-full bringin-slider-story-rounded-full bringin-slider-story-flex bringin-slider-story-items-center bringin-slider-story-justify-center' onClick={()=> handleOpenModal(playlist.id, idx)}>
                <img className='bringin-slider-story-rounded-full bringin-slider-story-w-[inherit] bringin-slider-story-h-[inherit] bringin-slider-story-object-cover' src={`${playlist?.thumbnail?CND_URL+ "/" + playlist.thumbnail :""}`}/>
              </div>
          </swiper-slide>
            )
        })
       }

      </swiper-container>

      {/* {isModalOpen ? (
        <SwiperModal
        // playlistID={playlistID}
        isModalOpen={isModalOpen}
        playlistResponse={playlistResponse}
        handleCloseModal={handleCloseModal}
        currentVideo={currentVideo}
        setCurrentVideo={setCurrentVideo}
        currentIndex={currentIndex}
        setCurrentIndex={setCurrentIndex}
        descriptionBoxRef={descriptionBoxRef}
        controlsVideoRef={controlsVideoRef}
        viewportHeigth={viewPortHeight}
        checkoutEnabled={checkoutEnabled}
        addToCartEnabled={addToCartEnabled}
        checkoutType={checkoutType}
        trackEvent={trackEvent}
        />
      ) : (
        ""
      )} */}

      {
        isModalOpen ?
        <PlaylistModal
          isModalOpen={isModalOpen}
          handleCloseModal={handleCloseModal}
          playlistResponse={playlistResponse}
          currentPlaylistId={currentPlaylistId}
          setCurrentPlaylistId={setCurrentPlaylistId}
          currentActivePlaylistIndex = {currentActivePlaylistIndex}
          setCurrentActivePlaylistIndex = {setCurrentActivePlaylistIndex}
          viewPortHeight={viewPortHeight}
          checkoutEnabled={checkoutEnabled}
          addToCartEnabled={addToCartEnabled}
          checkoutType={checkoutType}
        />
        :""
      }
    </div>
  )
}

export default StorySlider
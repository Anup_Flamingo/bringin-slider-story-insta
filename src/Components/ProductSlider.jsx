import {
  faGreaterThan,
  faXmark,
  faCartShopping,
  faCircleCheck,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import React, { useEffect, useRef, useState } from "react";
// import function to register Swiper custom elements
import { register } from "swiper/element/bundle";

// register Swiper custom elements
register();

const productModalStyle = {
  products: {
    height: "fit-content",
    padding: "8px",
    cursor: "pointer",
    borderRadius: "5px",
    background: "rgba(0,0,0,0.7)",
    color: "white",
    fontSize: "16px",
    display: "flex",
    gap: "10px",
  },
};

function ProductSlider(props) {
  const {
    videoObj,
    showDescription,
    setShowDescription,
    checkoutEnabled,
    addToCartEnabled,
    checkoutType,
  } = props;

  const [selectedProduct, setSelectedProduct] = useState(null);
  const [selectedVariantIndex, setSelectedVariantIndex] = useState(0);
  const [toggleDescription, setToggleDescription] = useState(false);
  const [moreDes, setMoreDes] = useState(false);
  const [varinatSelected, setVariantSelected] = useState(null);
  const [cartCount, setCartCount] = useState(0)


  const productSwiperRef = useRef(null);

  useEffect(() => {
    setTimeout(() => {
      if (productSwiperRef.current) {
        const windowWidth = window.innerWidth;
        const slidesPerViewport = windowWidth > 500 ? 3 : windowWidth / 190;

        const productSwiperParams = {
          breakpoints: {
            0: {
              slidesPerView: 1,
              spaceBetween: 10,
            },
            350: {
              slidesPerView: 2,
              spaceBetween: 10,
            },
          },
          slidesPerView: slidesPerViewport,
        };

        Object.assign(productSwiperRef.current, productSwiperParams);
        productSwiperRef.current.initialize();

        console.log(
          // productSwiperRef.current.swiper.pagination
          productSwiperRef.current.swiper.navigation
        );
        // Navigation Styling
        const navigationDiv = productSwiperRef.current.swiper.navigation;
        const prevEl = navigationDiv?.prevEl;
        const nextEl = navigationDiv?.nextEl;
        if (prevEl && nextEl) {
          nextEl.style.width = "27px";
          nextEl.style.height = "27px";
          nextEl.style.backgroundColor = "#0003";
          nextEl.style.marginTop = "-13px";
          nextEl.style.borderRadius = "50%";
          nextEl.children[0].style.width = "8px";
          nextEl.children[0].style.color = "white";

          prevEl.style.width = "27px";
          prevEl.style.height = "27px";
          prevEl.style.backgroundColor = "#0003";
          prevEl.style.marginTop = "-9px";
          prevEl.style.borderRadius = "50%";
          prevEl.children[0].style.width = "8px";
          prevEl.children[0].style.color = "white";
        }
      }
    }, 0);
  }, []);

  useEffect(()=>{
    if(!showDescription){
        handleCloseDesc()
    }
  },[showDescription])

  const handleOpenDescription = (event, product) => {
    setShowDescription(true);
    setSelectedProduct(product);
  };

  const handleCloseDesc = () => {
    setShowDescription(false);
    setSelectedProduct(null);
    setToggleDescription(false)
  };

  const handleAddToCart = (event) => {
    event.preventDefault();
    setToggleDescription(true);
    const variant = selectedProduct?.variants[0];
    const variantId = variant?.id;
    setVariantSelected({...variant})
    document.getElementById(`btnradio_${variantId}`).checked = true;
  };

  const handleVariantChange = (index)=>{
    const variant = selectedProduct?.variants[index];
    const variantId = variant?.id;
    setVariantSelected({...variant})
    document.getElementById(`btnradio_${variantId}`).checked = true;
  }

  const handleRedirectProductDespcriptionPage = ()=>{
    // Omly Track Events were there 
  }



  const handleDone = (index) => {
    try {
      const variant = selectedProduct?.variants[selectedVariantIndex]
      const payload = {
        id: variant?.variant_id,
        quantity: 1,
      };

      // Start: Testing Mode
      // setToggleDescription(false);
      // setVariantSelected({ ...variant });
      // End: Testing Mode

      // Start:-- Production Mode
      axios
        .post("/cart/add.js", payload)
        .then((res) => {
          if (res?.status === 200) {
            const count = res.data.quantity;
            count && setCartCount(count);
            setToggleDescription(false);
            setVariantSelected({ ...variant });
          }
        })
        .catch((err) => {});
      // End:-- Production Mode
    } catch (err) {}
  };


  return (
    <div
      className={`bringinProductStorySlider bringin-slider-story-w-full  bringin-slider-story-absolute bringin-slider-story-bottom-0 ${
        showDescription ? "" : "bringin-slider-story-pb-[20px]"
      }`}
    >
      <div
        className={`${showDescription ? "bringin-slider-story-hidden" : ""}`}
      >
        <swiper-container
          ref={productSwiperRef}
          nested={true}
          spaceBetween={20}
          navigation={true}
          init="true"
          centered-slides="true"
        >
          {videoObj.products.map((prod, index) => {
            return (
              <swiper-slide
                style={productModalStyle.products}
                key={prod?.id}
                onClick={(event) => handleOpenDescription(event, prod)}
              >
                <img
                  className="bringin-slider-story-w-[60px] bringin-slider-story-h-[60px] bringin-slider-story-rounded bringin-slider-story-object-cover "
                  alt={prod?.images[0]?.alt}
                  src={prod?.images[0]?.src}
                />
                <div className="bringin-slider-story-flex bringin-slider-story-flex-col bringin-slider-story-justify-between bringin-slider-story-w-[70%] md:bringin-slider-story-w-[60%] xxlg:bringin-slider-story-w-[63%] 5xl:bringin-slider-story-w-[70%] bringin-slider-story-text-[14px] ">
                  <div className="prodTitle bringin-slider-story-overflow-hidden bringin-slider-story-whitespace-nowrap bringin-slider-story-text-ellipsis ">
                    {prod?.title}
                  </div>
                  <div>{`₹ ${prod?.variants[0]?.price} `}</div>
                  <div>Shop Now {`>`}</div>
                </div>
              </swiper-slide>
            );
          })}
        </swiper-container>
      </div>

      {/* Description Box  */}
      {showDescription ? (
        <article
          className={`bringinSliderStroyDescriptionBoxMain bringin-slider-story-w-full bringin-slider-story-max-h-[65vh] bringin-slider-story-flex bringin-slider-story-justify-center bringin-slider-story-absolute bringin-slider-story-bottom-0 xl:bringin-slider-story-bottom-[0%] 2xl:bringin-slider-story-bottom-[0%] 3xl:bringin-slider-story-bottom-[0%] bringin-slider-story-text-sm bringin-slider-story-text-white`}
        >
          <div className="bringin-slider-story-w-full bringin-slider-story-bg-black/80 bringin-slider-story-rounded-t-2xl bringin-slider-story-pt-4  bringin-slider-story-pb-[30px] bringin-slider-story-px-4">
            <div className="bringin-slider-story-flex bringin-slider-story-justify-between bringin-slider-story-items-center bringin-slider-story-pb-2 bringin-slider-story-border-2 bringin-slider-story-border-solid bringin-slider-story-border-transparent bringin-slider-story-border-b-white  bringin-slider-story-pr-3 bringin-slider-story-font-bold">
              <div></div>
              <div className="bringin-slider-story-tracking-wide bringin-slider-story-text-lg">
                Shop
              </div>
              <button
                className="bringin-slider-story-cursor-pointer bringin-slider-story-border-none bringin-slider-story-bg-transparent bringin-slider-story-text-white bringin-slider-story-text-xl"
                onClick={handleCloseDesc}
              >
                <FontAwesomeIcon icon={faXmark} />
              </button>
            </div>
            <a
              className="bringin-slider-story-w-full bringin-slider-story-no-underline bringin-slider-story-text-white"
              href={`${selectedProduct?.handle}`}
              //   onClick={handleRedirectProductDespcriptionPage}
              target="_blank"
              rel="noreferrer"
            >
              <div className="bringin-slider-story-flex bringin-slider-story-items-center bringin-slider-story-gap-2 bringin-slider-story-text-sm bringin-slider-story-pt-2 bringin-slider-story-font-bold">
                <img
                  src={selectedProduct?.images[0]?.src}
                  alt={selectedProduct?.title}
                  className="bringin-slider-story-rounded"
                  width="50px"
                  height="50px"
                />
                <div>
                  {selectedProduct?.title}
                  <br />
                  {`₹ ${selectedProduct?.variants[selectedVariantIndex]?.price}`}
                </div>
              </div>
            </a>
            <section
              className="bringin-slider-story-transition bringin-slider-story-duration-150 bringin-slider-story-ease-out"
              style={{
                display: toggleDescription ? "none" : "block",
              }}
            >
              <div className="bringin-slider-story-pt-2 bringin-slider-story-text-sm">
                <div className="bringin-slider-story-font-bold bringin-slider-story-mb-2">
                  Description
                </div>
                <div
                  className={`bringinDescriptionBox bringin-slider-story-transition-height bringin-slider-story-text-white bringin-slider-story-duration-200 bringin-slider-story-ease-linear bringin-slider-story-text-justify bringin-slider-story-overflow-y-scroll
                ${
                  moreDes
                    ? "bringin-slider-story-max-h-40 bringin-slider-story-overflow-y-scroll"
                    : "bringin-slider-story-max-h-16 bringin-slider-story-overflow-y-scroll"
                }`}
                  dangerouslySetInnerHTML={{
                    __html: selectedProduct?.description,
                  }}
                ></div>
              </div>
              <div
                className="bringin-slider-story-text-center bringin-slider-story-cursor-pointer bringin-slider-story-mt-4 bringin-slider-story-font-bold hover:bringin-slider-story-text-white/80"
                onClick={() => setMoreDes((prev) => !prev)}
              >
                {selectedProduct?.description?.length > 0
                  ? moreDes
                    ? "Less"
                    : "More"
                  : ""}
              </div>
              {varinatSelected ? (
              <div className="bringin-slider-story-w-full bringin-slider-story-flex bringin-slider-story-gap-4">
                <a
                  className={`${
                    checkoutEnabled === "true"
                      ? "bringin-slider-story-w-1/2"
                      : "bringin-slider-story-w-full"
                  } bringin-slider-story-no-underline`}
                  onClick={handleRedirectProductDespcriptionPage}
                  href="/cart"
                >
                  <button id="bringinAddedToCart" className="bringin-slider-story-w-full bringin-slider-story-border-none bringin-slider-story-bg-white bringin-slider-story-text-black bringin-slider-story-mt-2 bringin-slider-story-mb-2 hover:bringin-slider-story-bg-white/90">
                    <FontAwesomeIcon
                      color="#198754"
                      icon={faCircleCheck}
                    />{" "}
                    Added to Cart
                  </button>
                </a>
                {checkoutType === "gokwik" ? (
                  <div className="gokwik-checkout bringin-slider-story-w-1/2 bringin-slider-story-mb-0">
                    <button
                      id="bringinCheckout"
                      type="button"
                      className="bringin-slider-story-w-full bringin-slider-story-border-none bringin-slider-story-mt-2 bringin-slider-story-mb-2 bringin-slider-story-relative"
                      ref={checkoutGokwikRef}
                      // onClick= "onCheckoutClick(this)"
                      onClick= {(e)=>{console.log(checkoutGokwikRef.current); window.onCheckoutClick(checkoutGokwikRef.current);}}
                    >
                      <span style={{color: "inherit"}} className="btn-text">
                        <span style={{fontWeight:"inherit", fontSize:"inherit"}}>Checkout {" "}
                      <FontAwesomeIcon icon={faCartShopping} />{" "} 
                      <span
                        id="cartCountBadge"
                        style={{
                          top: "-13px",
                          right: "-12px",
                        }}
                        className="bringin-slider-story-absolute bringin-slider-story-rounded-full bringin-slider-story-w-7 bringin-slider-story-h-7 bringin-slider-story-flex bringin-slider-story-justify-center bringin-slider-story-items-center bringin-slider-story-right-0 bringin-slider-story-border bringin-slider-story-border-white"
                      >
                        {cartCount}
                      </span>
                       </span>
                        <span></span>
                      </span>
                      <span className="pay-opt-icon bringin-slider-story-hidden">
                      </span>
                      <div className="bringin-slider-story-hidden">
                        <div className="cir-loader">Loading..</div>
                      </div>
                    </button>
                  </div>
                ) : (
                  <a
                    className={`${
                      checkoutEnabled === "true"
                        ? "bringin-slider-story-w-1/2"
                        : "bringin-slider-story-hidden"
                    } bringin-slider-story-no-underline`}
                    href="/checkout"
                  >
                    <button
                      id="bringinCheckout"
                      className="bringin-slider-story-w-full bringin-slider-story-border-none bringin-slider-story-mt-2 bringin-slider-story-mb-2 bringin-slider-story-relative"
                    >
                      Checkout{" "}
                      <FontAwesomeIcon icon={faCartShopping} />{" "}
                      <span
                        id="cartCountBadge"
                        style={{
                          top: "-13px",
                          right: "-12px",
                        }}
                        className="bringin-slider-story-absolute bringin-slider-story-rounded-full bringin-slider-story-w-7 bringin-slider-story-h-7 bringin-slider-story-flex bringin-slider-story-justify-center bringin-slider-story-items-center bringin-slider-story-right-0 bringin-slider-story-border bringin-slider-story-border-white"
                      >
                        {cartCount}
                      </span>
                    </button>
                  </a>
                )}
              </div>
            ) : !varinatSelected &&
              addToCartEnabled === "true" ? (
              <button
                id="bringinAddToCart"
                className="bringin-slider-story-w-full bringin-slider-story-border-none bringin-slider-story-mt-2 bringin-slider-story-mb-2"
                onClick={(event) => handleAddToCart(event)}
              >
                Add to Cart
              </button>
            ) : (
              ""
            )}
            </section>

            {/* Variant Section */}
            <section
            style={{
              display: !toggleDescription ? "none" : "block",
            }}
            className="bringin-slider-story-mt-4"
          >
            <article className="bringin-slider-story-flex bringin-slider-story-justify-center bringin-slider-story-flex-wrap bringin-slider-story-gap-3">
              {selectedProduct?.variants?.map((variant, idt) => {
                return (
                  <div key={variant?.id} className="bringin-slider-story-mt-2">
                    <input
                      type="radio"
                      value={variant.variant_id}
                      className="btnVariantRadio bringin-slider-story-hidden"
                    //   name={`btnradio_${index}`}
                      id={`btnradio_${variant.id}`}
                      onChange={(event) =>
                        handleVariantChange(idt)
                      }
                    />
                    <label
                      className="btnVariant bringin-slider-story-cursor-pointer"
                      htmlFor={`btnradio_${variant.id}`}
                    >
                      {variant.title}
                    </label>
                  </div>
                );
              })}
            </article>
            <div className="bringin-slider-story-flex bringin-slider-story-justify-center bringin-slider-story-mt-3">
              <button
                className=" bringin-slider-story-w-fit bringin-slider-story-border-none bringin-slider-story-text-black bringin-slider-story-p-2 bringin-slider-story-text-base bringin-slider-story-bg-white bringin-slider-story-border-white bringin-slider-story-rounded bringin-slider-story-mt-2 bringin-slider-story-mb-2 bringin-slider-story-px-5 bringin-slider-story-py-2  hover:bringin-slider-story-bg-white/90"
                onClick={() => handleDone()}
              >
                Done
              </button>
            </div>
          </section>
          </div>
        </article>
      ) : (
        ""
      )}
    </div>
  );
}

export default ProductSlider;

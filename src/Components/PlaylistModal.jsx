import React, { createRef, useEffect, useRef, useState } from "react";
import Modal from "react-modal";
// import function to register Swiper custom elements
import { register } from "swiper/element/bundle";
import { CND_URL } from "../config";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPlay,
  faVolumeHigh,
  faVolumeMute,
  faXmark,
} from "@fortawesome/free-solid-svg-icons";
import { RotatingLines } from "react-loader-spinner";
import ProductSlider from "./ProductSlider";

// register Swiper custom elements
register();

Modal.setAppElement("#storySliderRoot");

const customStyles = {
  overlay: {
    backgroundColor: "rgba(0, 0, 0, 0.7)",
    zIndex: 100000000000,
    overflow: "hidden",
    // height: "-webkit-fill-available",
    height: "100%",
  },
  content: {
    top: "0%",
    left: "0%",
    right: "0%",
    bottom: "0%",
    padding: 0,
    backgroundColor: "transparent",
    border: "1ps solid transparent",
    overflow: "hidden",
    height: "100%",
    zIndex: 10,
  },
};

function PlaylistModal(props) {
  const {
    isModalOpen,
    currentPlaylistId,
    setCurrentPlaylistId,
    viewPortHeight,
    playlistResponse,
    currentActivePlaylistIndex,
    setCurrentActivePlaylistIndex,
    handleCloseModal,
    checkoutEnabled, 
    addToCartEnabled, 
    checkoutType
  } = props;

  const [activeVideoIndex, setCurrentActiveVideoIndex] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [showDescription, setShowDescription] = useState(false)

  const playlistModalSwiperRef = useRef(null);
  const playlistVideoSwiperRef = useRef(null);

  if (!playlistVideoSwiperRef.current) {
    playlistVideoSwiperRef.current = playlistResponse.map((item) =>
      createRef()
    );
  }

  useEffect(() => {
    setTimeout(() => {
      // Initializing the Playlist Modal Parent Swiper
      // Adding Events for each Playlist Slide
      if (playlistModalSwiperRef.current) {
        const playlistSwiperParams = {
          on: {
            slideChange: (event) => {
              setShowDescription(false)
              const active_index = event.activeIndex;
              const prev_index = event.previousIndex;

              if (
                active_index > prev_index &&
                Math.abs(active_index - prev_index) === 1
              ) {
                //Next Slide
                console.log("Next Playlist Slide ", active_index);
                const videoSliderSwiper =
                  event.slides[prev_index].querySelector("swiper-container");
                if (videoSliderSwiper?.swiper?.slides?.length > 0) {
                  const lastIndexOfVideo =
                    videoSliderSwiper.swiper.slides.length - 1;
                  setCurrentActivePlaylistIndex(active_index); //Changing Playlist Active Index
                  setCurrentActiveVideoIndex(0); // Setting Current Active Index to Zero when the Playlist Swiper is Slided

                  // Sliding the Video Slider to the Active Index  of the Video
                  // videoSliderSwiper.swiper.slideTo(0, false)

                  // Pause Last Video of Previous Active Playlist
                  handlePauseVideo(prev_index, lastIndexOfVideo);
                  // Play First Video  of Current Active Playlist
                  handlePlayVideo(active_index, 0);
                }
              } else if (
                active_index < prev_index &&
                Math.abs(active_index - prev_index) === 1
              ) {
                //Slide Prev
                console.log("Prev Playlist Slide ", active_index);
                const videoSliderSwiper =
                  event.slides[active_index].querySelector("swiper-container");
                const lastIndexOfVideo =
                  videoSliderSwiper.querySelectorAll(
                    "swiper-container swiper-slide"
                  ).length - 1;
                setCurrentActivePlaylistIndex(active_index); //Changing Playlist Active Index
                setCurrentActiveVideoIndex(lastIndexOfVideo); // Current Active Playlist's Last video will be active

                // Sliding the Video Slider to the Active Index  of the Video
                videoSliderSwiper.swiper.slideTo(lastIndexOfVideo, false);

                // Pause the First Video of Previous Active Playlist
                handlePauseVideo(prev_index, 0);
                // Play the last Video  of Current Active Playlist
                handlePlayVideo(active_index, lastIndexOfVideo);
              } else {
              }
            },
          },
        };

        Object.assign(playlistModalSwiperRef.current, playlistSwiperParams);
        playlistModalSwiperRef.current.initialize();
        playlistModalSwiperRef.current.swiper.slideTo(
          currentActivePlaylistIndex,
          false
        );
      }

      // Initializing Each Video inside the Playlist Swiper
      // Adding Events for each Video in Playlist Swiper
      playlistVideoSwiperRef.current?.forEach((videoSwiper) => {
        const videoSwiperParams = {
          on: {
            slideChangeTransitionEnd: (event) => {
              setShowDescription(false) //Description is Hidded
              // console.log("Video Slider Channged");
              const active_index = event.activeIndex;
              const prev_index = event.previousIndex;
              setCurrentActiveVideoIndex(active_index); //Changing Video Active Index

              // Getting Active Index of the Playlist Slider
              const active_paylist =
                playlistModalSwiperRef.current.swiper.activeIndex;

              // Pause Previous Video
              handlePauseVideo(active_paylist, prev_index);
              // Play Current Video
              handlePlayVideo(active_paylist, active_index);
              
            },
          },
        };
        Object.assign(videoSwiper.current, videoSwiperParams);
        videoSwiper?.current?.initialize();
      });

      // Playing the First Video (Index = 0) of the current Active Playlsit Index
      handlePlayVideo(currentActivePlaylistIndex, 0);
    }, 100);
  }, []);

  useEffect(() => {
    console.log("currentActivePlaylistIndex", currentActivePlaylistIndex);
  }, [currentActivePlaylistIndex]);

  const handlePlayVideo = (currentPlaylist_IDX, currentVideoPlaylist_IDX) => {
    const currentVideoSlide =
      playlistVideoSwiperRef.current[currentPlaylist_IDX]?.current?.swiper
        ?.slides[currentVideoPlaylist_IDX];

    const currentVideo = currentVideoSlide?.querySelector("swiper-slide video");
    if (currentVideo) {
      const mainPlayPauseDiv = currentVideoSlide?.querySelector(
        ".bringinStorySliderPlayPauseVideoDiv"
      );
      currentVideo.play();
      mainPlayPauseDiv?.classList?.add("bringin-slider-story-hidden");
      mainPlayPauseDiv?.classList?.remove("bringin-slider-story-flex");

      // console.log("Played --", currentVideo.getAttribute('title'));
    }
  };
  const handlePauseVideo = (currentPlaylist_IDX, currentVideoPlaylist_IDX) => {
    const currentVideoSlide =
      playlistVideoSwiperRef.current[currentPlaylist_IDX].current?.swiper
        ?.slides[currentVideoPlaylist_IDX];

    const currentVideo = currentVideoSlide?.querySelector("swiper-slide video");

    if (currentVideo) {
      const mainPlayPauseDiv = currentVideoSlide?.querySelector(
        ".bringinStorySliderPlayPauseVideoDiv"
      );

      currentVideo.pause();
      mainPlayPauseDiv?.classList?.remove("bringin-slider-story-hidden");
      mainPlayPauseDiv?.classList?.add("bringin-slider-story-flex");
      // console.log("Paused --", currentVideo.getAttribute('title'));
    }
  };

  const handleVideoProgressingBar = (
    currentPlaylist_IDX,
    currentVideoPlaylist_IDX
  ) => {
    const currentPlaylistSlide =
      playlistModalSwiperRef.current.swiper.slides[currentPlaylist_IDX];
    const curentVideoSlide =
      playlistVideoSwiperRef.current[currentPlaylist_IDX].current.swiper.slides[
        currentVideoPlaylist_IDX
      ];

    const currentVideo = curentVideoSlide.querySelector("swiper-slide video");

    const allProgressBar = currentPlaylistSlide.querySelectorAll(
      ".bringinSingleVideoProgressbarDiv"
    );

    // Filling all the Progress Bar with white spaces for the (currentVideoPlaylist_IDX - 1)
    allProgressBar?.forEach((progressBar, idx) => {
      if (idx < currentVideoPlaylist_IDX) {
        progressBar.querySelector(
          ".bringinVideoProgressbarProgressingDiv"
        ).style.width = "100%";
      } else if (idx === currentVideoPlaylist_IDX) {
        const videoDurataion = currentVideo.duration;
        progressBar.querySelector(
          ".bringinVideoProgressbarProgressingDiv"
        ).style.width = `${(currentVideo.currentTime / videoDurataion) * 100}%`;
      } else {
        progressBar.querySelector(
          ".bringinVideoProgressbarProgressingDiv"
        ).style.width = "0%";
      }
    });
  };

  const handleMuteUnmuteVideo = (
    currentPlaylist_IDX,
    currentVideoPlaylist_IDX
  ) => {
    const currentVideoSlide =
      playlistVideoSwiperRef.current[currentPlaylist_IDX].current.swiper.slides[
        currentVideoPlaylist_IDX
      ];

    const currentVideo = currentVideoSlide.querySelector("swiper-slide video");
    const muteUnmuteDiv = currentVideoSlide.querySelector(
      "swiper-slide .bringinSliderMuteUnmuteVideo"
    );
    if (currentVideo.muted) {
      currentVideo.muted = false;
      muteUnmuteDiv
        .querySelector(".bringinVideoMute")
        .classList.add("bringin-slider-story-hidden");
      muteUnmuteDiv
        .querySelector(".bringinVideoUnMute")
        .classList.remove("bringin-slider-story-hidden");
    } else {
      currentVideo.muted = true;
      muteUnmuteDiv
        .querySelector(".bringinVideoMute")
        .classList.remove("bringin-slider-story-hidden");
      muteUnmuteDiv
        .querySelector(".bringinVideoUnMute")
        .classList.add("bringin-slider-story-hidden");
    }
  };

  const handlePlayPauseVideo = (
    currentPlaylist_IDX,
    currentVideoPlaylist_IDX
  ) => {
    const currentVideoSlide =
      playlistVideoSwiperRef.current[currentPlaylist_IDX].current.swiper.slides[
        currentVideoPlaylist_IDX
      ];

    const currentVideo = currentVideoSlide.querySelector("swiper-slide video");

    if (currentVideo.paused) {
      handlePlayVideo(currentPlaylist_IDX, currentVideoPlaylist_IDX);
    } else {
      handlePauseVideo(currentPlaylist_IDX, currentVideoPlaylist_IDX);
    }
  };

  return (
    <Modal isOpen={isModalOpen} style={customStyles}>
      <section
        style={{ height: `${viewPortHeight}px`, maxWidth: "400px" }}
        id="playlistModalSection"
        className="bringin-slider-story-mx-auto"
      >
        {/* Outer Swiper Container */}
        <swiper-container
          ref={playlistModalSwiperRef}
          slides-per-view={"1"}
          spaceBetween={0}
          init="false"
        >
          {playlistResponse.map((playlist, index) => {
            return (
              <swiper-slide key={playlist.id}>
                <section className="bringin-slider-story-relative bringin-slider-story-h-full">
                  <div className="bringinAllVideosProgressBarMainDiv bringin-slider-story-h-[20px] bringin-slider-story-z-10 bringin-slider-story-absolute bringin-slider-story-flex bringin-slider-story-w-full bringin-slider-story-gap-[5px] bringin-slider-story-py-[10px] bringin-slider-story-px-[5px]">
                    {playlist.videos.map((video) => {
                      const windowWidth =
                        window.innerWidth <= 800 ? window.innerWidth : 800;
                      const autoPlaySlideWidth =
                        windowWidth / playlist.videos.length;
                      const totalSlideShowWidth =
                        (autoPlaySlideWidth / windowWidth) * 100;
                      return (
                        <div
                          key={video.id}
                          style={{ width: `${totalSlideShowWidth}%` }}
                          className="bringinSingleVideoProgressbarDiv bringin-slider-story-relative"
                        >
                          <div style={{display: "block"}} className="bringinVideoProgressbarBackground bringin-slider-story-bg-[#b8b8b8] bringin-slider-story-absolute bringin-slider-story-top-0 bringin-slider-story-h-[8px] bringin-slider-story-w-full bringin-slider-story-rounded-[5px]"></div>
                          <div style={{display: "block"}} className="bringinVideoProgressbarProgressingDiv bringin-slider-story-bg-white bringin-slider-story-absolute bringin-slider-story-top-0 bringin-slider-story-left-[1px] bringin-slider-story-h-[8px]  bringin-slider-story-rounded-[5px]"></div>
                        </div>
                      );
                    })}
                  </div>
                  <div className="bringin-slider-story-absolute bringin-slider-story-w-full bringin-slider-story-h-[inherit]">
                    <swiper-container
                      nested={true}
                      slides-per-view={"1"}
                      spaceBetween={0}
                      ref={playlistVideoSwiperRef.current[index]}
                      init="false"
                    >
                      {playlist.videos.length > 0 &&
                        playlist.videos.map((video, idx) => {
                          return (
                            <swiper-slide key={video.id}>
                              <div className="bringin-slider-story-w-full bringin-slider-story-h-full bringin-slider-story-relative">
                                <video
                                  className="bringin-slider-story-block bringin-slider-story-w-full bringin-slider-story-h-full bringin-slider-story-object-contain bringin-slider-story-overflow-clip"
                                  // muted
                                  // controls
                                  title={video.title}
                                  playsInline
                                  preload="none"
                                  controlsList="nodownload"
                                  onTimeUpdate={(event) => {
                                    // console.log(`Playing Video Title --- ${video.title}, PlaylistIDx ---${index}, VideoIdx --- ${idx}`);
                                    if (
                                      event.target.duration ===
                                      event.target.currentTime
                                    ) {
                                      console.log("Completed");
                                      if (idx === playlist.videos.length - 1) {
                                        // Slide the Playlsit Slider
                                        playlistModalSwiperRef.current.swiper.slideNext();
                                      } else {
                                        // Slide the Video
                                        playlistVideoSwiperRef.current[
                                          index
                                        ].current.swiper.slideNext();
                                      }
                                    }

                                    handleVideoProgressingBar(index, idx);
                                  }}
                                  onWaiting={() => {
                                    // When Video is Loading.....
                                    setIsLoading(true);
                                  }}
                                  onPlaying={() => {
                                    console.warn("Playing:--- ", video.title);
                                    setIsLoading(false);
                                  }}
                                  onClick={() =>
                                    handlePlayPauseVideo(index, idx)
                                  }
                                >
                                  <source
                                    src={`${CND_URL}/${video.video_url}`}
                                    type="video/mp4"
                                  />
                                </video>

                                <div className="bringin-slider-story-absolute bringin-slider-story-top-[30px] bringin-slider-story-flex bringin-slider-story-justify-between bringin-slider-story-px-[5px] bringin-slider-story-w-full">
                                  <div
                                    className="bringinSliderMuteUnmuteVideo bringin-slider-story-cursor-pointer bringin-slider-story-w-12 bringin-slider-story-h-12 bringin-slider-story-rounded-full bringin-slider-story-bg-black/20 bringin-slider-story-flex bringin-slider-story-justify-center bringin-slider-story-items-center"
                                    onClick={() =>
                                      handleMuteUnmuteVideo(index, idx)
                                    }
                                  >
                                    <span className="bringinVideoMute bringin-slider-story-hidden">
                                      <FontAwesomeIcon
                                        color="#ffffff"
                                        icon={faVolumeMute}
                                      />
                                    </span>
                                    <span className="bringinVideoUnMute">
                                      <FontAwesomeIcon
                                        color="#ffffff"
                                        icon={faVolumeHigh}
                                      />
                                    </span>
                                  </div>

                                  {/* <div className='bringin-slider-story-text-red-800'>
                          <span>active_index = {index}</span>
                          </div> */}
                                  <div
                                    className="bringinSliderCloseVideo bringin-slider-story-cursor-pointer bringin-slider-story-w-12 bringin-slider-story-h-12 bringin-slider-story-rounded-full bringin-slider-story-bg-black/20 bringin-slider-story-flex bringin-slider-story-justify-center bringin-slider-story-items-center "
                                    onClick={handleCloseModal}
                                  >
                                    <FontAwesomeIcon
                                      color="#ffffff"
                                      icon={faXmark}
                                    />
                                  </div>
                                </div>

                                {isLoading ? (
                                  <div
                                    style={{
                                      top: "50%",
                                      left: "50%",
                                      marginTop: "-40px",
                                      marginLeft: "-40px",

                                      // transform: "translate(171%, -50%)",
                                    }}
                                    className="bringin-slider-story-absolute"
                                  >
                                    <RotatingLines
                                      strokeColor="white"
                                      strokeWidth="5"
                                      animationDuration="0.75"
                                      width="80"
                                      visible={true}
                                    />
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>

                              <div
                                style={{
                                  top: "50%",
                                  left: "50%",
                                  marginLeft: "-40px",
                                  marginTop: "-40px",
                                  marginRight: 0,
                                  marginBottom: 0,
                                }}
                                className="bringinStorySliderPlayPauseVideoDiv bringin-slider-story-absolute bringin-slider-story-w-[80px] bringin-slider-story-h-[80px] bringin-slider-story-rounded-full bringin-slider-story-bg-black/20 bringin-slider-story-justify-center bringin-slider-story-items-center bringin-slider-story-hidden"
                                onClick={() => handlePlayPauseVideo(index, idx)}
                              >
                                <FontAwesomeIcon
                                  color="#ffffff"
                                  fontSize={40}
                                  icon={faPlay}
                                />
                              </div>
                              {video.products?.length > 0 ? (
                                  <ProductSlider videoObj={video} showDescription={showDescription} setShowDescription={setShowDescription}                      
                                checkoutEnabled={checkoutEnabled}
                                addToCartEnabled={addToCartEnabled}
                                checkoutType={checkoutType}
                                  />
                              ) : (
                                ""
                              )}
                            </swiper-slide>
                          );
                        })}
                    </swiper-container>
                  </div>
                </section>
              </swiper-slide>
            );
          })}
        </swiper-container>
      </section>
    </Modal>
  );
}

export default PlaylistModal;

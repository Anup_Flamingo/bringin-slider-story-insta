import { useEffect, useState, useRef, createRef } from "react";
import waitForElementToDisplay from "./Methods/MountUI";
import verge from "verge";
import axios from "axios";
import {
  API_HOST,
  PLAUSIBLE_DOMAIN,
  PRODUCT_URL,
  PlayLists_URL,
  CND_URL,
} from "./config";
import StorySlider from "./Components/StorySlider";
import "./style/DummyStyling.css";

function App() {
  const [viewPortHeight, setViewPortHeigth] = useState(verge.viewportH());

  // State Management-- PlayistID & ProductID
  // Production
  const [mounted, setMounted] = useState(false);
  const [playlistIdList, setPlaylistIdList] = useState("");
  const [mainThemeColor, setMainThemeColor] =  useState("")
  const [playlistResponse, setPlaylistResponse] = useState(null);
  // Production

  // Testing
  // const [mounted, setMounted] = useState(true);
  // const [playlistIdList, setPlaylistIdList] = useState("51,49,61,54,58,59,60"); //61, 59, 53, 19, 42, 50,54,58,59,60
  // const [playlistResponse, setPlaylistResponse] = useState(null);
  // const [mainThemeColor, setMainThemeColor] = useState("");
  // Testing

  const [checkoutEnabled, setCheckoutEnabled] = useState("true");
  const [addToCartEnabled, setAddToCartEnabled] = useState("true");
  const [checkoutType, setCheckoutType] = useState("shopify");

  const descriptionBoxRef = useRef([]);
  const controlsVideoRef = useRef([]);

  useEffect(() => {
    waitForElementToDisplay(
      ".maBringinSliderStory",
      () => {
        setMounted(true);
      },
      100,
      5000
    );

    visualViewport.addEventListener("resize", () => {
      setViewPortHeigth(verge.viewportH());
    });
  }, []);

  // Parameter are Set after mounting
  useEffect(() => {
    if (mounted) {
      // Production
      setPlaylistIdList(document.querySelector(".maBringinSliderStory")?.dataset?.playlistidlist)
      setMainThemeColor(document.querySelector(".maBringinSliderStory")?.dataset?.mainthemecolor)
      setCheckoutEnabled(
        document.querySelector(".maBringinSliderStory")?.dataset?.checkoutenabled
      );
      setAddToCartEnabled(
        document.querySelector(".maBringinSliderStory")?.dataset?.addtocartenabled
      );
      setCheckoutType(
        document.querySelector(".maBringinSliderStory")?.dataset?.checkouttype
      );
      console.log("mounted");
      // Production
    }
  }, [mounted]);

  // Response through playlist ID
  useEffect(() => {
    if (playlistIdList) {
      axios.get(`${PlayLists_URL}/${playlistIdList}`).then((res) => {
        if (res.status === 200) {
          setPlaylistResponse(res.data);

          console.log(res.data);

          descriptionBoxRef.current =
            res.data.videos?.length > 0 &&
            res.data.videos.map((item) => createRef());

          controlsVideoRef.current =
            res.data.videos?.length > 0 &&
            res.data.videos.map((item) => createRef());
        }
      });
    }
  }, [playlistIdList]);

  return (
    <>
      {playlistResponse && playlistResponse.length > 0 ? (
        <div
          style={{ margin: "10px auto" }}
          className="bringin-slider-story-max-w-[1300px] bringin-slider-story-py-[10px] bringin-slider-story-px-[5px]"
        >
          <div className="bringin-slider-story-w-full bringin-slider-story-mx-auto">
            <StorySlider
              playlistResponse={playlistResponse}
              mainThemeColor={mainThemeColor}
              viewPortHeight={viewPortHeight}
              checkoutEnabled={checkoutEnabled}
              addToCartEnabled={addToCartEnabled}
              checkoutType={checkoutType}
            />
          </div>
        </div>
      ) : (
        ""
      )}
    </>
  );
}

export default App;
